pub use fql_macros::{fql, fql_at};
use fql_parse::{parse, Expr};
use ureq::Agent;

pub struct Client {
  agent:    Agent,
  endpoint: String,
  secret:   String,
}

impl Client {
  /// Creates a new FaunaDB client. The `endpoint` is the database endpoint. For
  /// the classic region, use `db.fauna.com`. For the US region, use
  /// `db.us.fauna.com`. See more at
  /// [the docs](https://docs.fauna.com/fauna/current/learn/understanding/region_groups).
  pub fn new(endpoint: &str, secret: &str) -> Self {
    Client { agent: ureq::agent(), endpoint: endpoint.into(), secret: secret.into() }
  }

  pub fn call_str(&self, expr: &str) -> Result<ureq::Response, ureq::Error> {
    self.call(parse::parse_expr(expr).unwrap())
  }

  pub fn call(&self, expr: Expr) -> Result<ureq::Response, ureq::Error> {
    let json = serde_json::to_string(&expr).unwrap();
    self
      .agent
      .post(&format!("https://{}", self.endpoint))
      .set("Authorization", &format!("Bearer {}", self.secret))
      .set("X-FaunaDB-API-Version", "4")
      .send(json.as_bytes())
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_call() {
    let client = Client::new(
      "db.fauna.com",
      // Read the token at runtime so that we can still run tests without a token.
      std::fs::read_to_string("../token.txt").unwrap().trim().into(),
    );
    client.call(fql_at!("create.fql")).unwrap();
    client.call(fql!(Match())).unwrap();
    panic!();
  }
}
