use proc_macro::TokenStream;
use proc_macro2::Span;
use quote::ToTokens;
use syn::{
  parse::{Parse, ParseStream, Result},
  punctuated::Punctuated,
  token::Brace,
  Attribute, Expr, Ident, LitStr, Type,
};

struct QueryFuncInput {
  variants: Punctuated<QueryFuncVariant, Token![,]>,
}

struct QueryFuncVariant {
  attrs: Vec<Attribute>,
  name:  Ident,
  args:  Punctuated<QueryFuncArg, Token![,]>,
  block: Option<Expr>,
}

struct QueryFuncArg {
  name: LitStr,
  ty:   Type,
}

impl Parse for QueryFuncInput {
  fn parse(input: ParseStream) -> Result<Self> {
    Ok(QueryFuncInput { variants: input.parse_terminated(QueryFuncVariant::parse)? })
  }
}

impl Parse for QueryFuncVariant {
  fn parse(input: ParseStream) -> Result<Self> {
    let attrs = Attribute::parse_outer(input)?;
    let name: Ident = input.parse()?;
    let args;
    parenthesized!(args in input);
    Ok(QueryFuncVariant {
      attrs,
      name,
      args: args.parse_terminated(QueryFuncArg::parse)?,
      block: if input.peek(Brace) { Some(input.parse()?) } else { None },
    })
  }
}

impl Parse for QueryFuncArg {
  fn parse(input: ParseStream) -> Result<Self> {
    let name = input.parse()?;
    input.parse::<Token![:]>()?;
    let ty = input.parse()?;
    Ok(QueryFuncArg { name, ty })
  }
}

pub fn query_func(input: TokenStream) -> TokenStream {
  let input = parse_macro_input!(input as QueryFuncInput);

  let name = input.variants.iter().map(|v| &v.name).collect::<Vec<_>>();
  let name_str = input.variants.iter().map(|v| v.name.to_string()).collect::<Vec<_>>();
  let arg_count = input.variants.iter().map(|v| v.args.len()).collect::<Vec<_>>();
  let optional_args = input
    .variants
    .iter()
    .map(|v| v.args.iter().filter(|a| is_option(a)).count())
    .collect::<Vec<_>>();
  let no_args = input
    .variants
    .iter()
    .map(|v| v.args.len() == 1 && is_unit(v.args.iter().next().unwrap()))
    .collect::<Vec<_>>();
  let has_varargs = input
    .variants
    .iter()
    .map(|v| !v.args.is_empty() && is_varargs(v.args.last().unwrap()))
    .collect::<Vec<_>>();
  let name_lower =
    input.variants.iter().map(|v| v.name.to_string().to_lowercase()).collect::<Vec<_>>();
  let doc_link = name_lower
    .iter()
    .map(|name| format!("https://docs.fauna.com/fauna/current/api/fql/functions/{}", name))
    .collect::<Vec<_>>();
  let attrs = input.variants.iter().map(|v| &v.attrs).collect::<Vec<_>>();
  let field_ty = input
    .variants
    .iter()
    .map(|v| v.args.iter().map(|a| &a.ty).collect::<Vec<_>>())
    .collect::<Vec<_>>();
  let arg = input
    .variants
    .iter()
    .map(|v| v.args.iter().map(|a| convert_arg(&a.name)).collect::<Vec<_>>())
    .collect::<Vec<_>>();
  let serialize_block = input.variants.iter().map(|v| {
    if let Some(b) = &v.block {
      b.to_token_stream()
    } else {
      let block = v.args.iter().map(|a| {
        let arg = convert_arg(&a.name);
        let arg_str = &a.name;
        if is_option(&a) {
          quote!(
            if let Some(v) = #arg {
              out.insert(#arg_str, to_value(v).unwrap());
            }
          )
        } else {
          quote!(out.insert(#arg_str, to_value(#arg).unwrap()))
        }
      });
      quote!(#(#block;)*)
    }
  });
  let parse_block = input.variants.iter().map(|v| {
    let name = &v.name;
    let expr = v
      .args
      .iter()
      .enumerate()
      .map(|(i, _)| {
        if i == v.args.len() - 1 || is_option(&v.args[i + 1]) {
          quote! {{
            let e = parser.parse()?;
            if parser.tokens().peek(0)?.is_punct(Punct::Comma) {
              parser.tokens().expect(Punct::Comma)?;
            }
            e
          }}
        } else {
          quote! {{
            let e = parser.parse()?;
            parser.tokens().expect(Punct::Comma)?;
            e
          }}
        }
      })
      .collect::<Vec<_>>();
    quote! {
      Ok(Self::#name(#(#expr),*))
    }
  });
  // This generates source for converting a QueryFunc into a TokenStream. It is
  // used by the fql-macros crate to produce a QueryFunc literal at compile time.
  let to_tokens = input.variants.iter().map(|v| {
    let name = &v.name;
    let convert_args = v
      .args
      .iter()
      .map(|a| {
        let arg = convert_arg(&a.name);
        match &a.ty {
          Type::Path(p) => {
            let ident = &p.path.segments.first().unwrap().ident;
            if ident == "Option" {
              // Options are simply skipped if the don't exist, but we want Some(v) or None.
              // So, this generates an if statement in the generated code, which will produce
              // different generated QueryFunc literals (the inner nested generated code).
              //
              // Allows us to expand ##v correctly
              let v = Ident::new("v", Span::call_site());
              let new_arg = Ident::new(&format!("new_{}", arg), Span::call_site());
              quote! {
                let #new_arg;
                if let Some(v) = #arg {
                  #new_arg = quote!(Some(##v));
                } else {
                  #new_arg = quote!(None);
                }
                let #arg = #new_arg;
              }
            } else {
              quote!()
            }
          }
          _ => quote!(),
        }
      })
      .collect::<Vec<_>>();
    let expr = v
      .args
      .iter()
      .map(|a| {
        let arg = convert_arg(&a.name);
        match &a.ty {
          Type::Tuple(_) => quote!((())),
          _ => quote!(##arg),
        }
      })
      .collect::<Vec<_>>();
    quote! {
      #(#convert_args)*
      _tokens.append_all(quote!(fql_parse::QueryFunc::#name(#(#expr),*)));
    }
  });

  let out = quote! {
    pub enum QueryFunc {
      #(
        #(#attrs)*
        #[doc = concat!(
          "See the [docs](",
          #doc_link,
          ").",
        )]
        #name(#(#field_ty),*),
      )*
    }

    impl Serialize for QueryFunc {
      fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
      where
          S: Serializer,
      {
        let mut out = HashMap::new();
        match self {
          #(
            Self::#name(#(#arg),*) => {
              #serialize_block
            }
          )*
        }
        let mut map = serializer.serialize_map(Some(out.len()))?;
        for (k, v) in out {
          map.serialize_entry(k, &v)?;
        }
        map.end()
      }
    }

    use crate::parse::{
      Parser,
      token::{Ident, ParseError, Punct}
    };
    use std::io;

    impl QueryFunc {
      pub fn parse<R: io::Read>(ident: Ident, parser: &mut Parser<R>) -> Result<QueryFunc, ParseError> {
        match ident.value() {
          #(#name_str => {#parse_block})*
          _ => Err(ParseError::undefined("function", ident))
        }
      }

      /// Returns the number of enum variants for that function name. This is for validation in
      /// `fql-macros`.
      pub fn arg_count(name: &str) -> Option<usize> {
        Some(match name {
          #(#name_str => #arg_count,)*
          _ => return None,
        })
      }

      /// Returns the number of optional enum variants for that function name. This is for
      /// generation in `fql-macros`.
      pub fn optional(name: &str) -> Option<usize> {
        Some(match name {
          #(#name_str => #optional_args,)*
          _ => return None,
        })
      }

      /// Returns `true` if the function takes no arguments. `arg_count` will still return `1`
      /// for this function.
      pub fn no_args(name: &str) -> Option<bool> {
        Some(match name {
          #(#name_str => #no_args,)*
          _ => return None,
        })
      }

      /// Returns the doc link for the given named function. Returns `None` if the name is invalid.
      pub fn doc_link(name: &str) -> Option<&'static str> {
        Some(match name {
          #(#name_str => #doc_link,)*
          _ => return None,
        })
      }

      /// Returns `true` if the given query function contains variadic arguments. If this is true,
      /// then the number of required arguments is `arg_count() - 1`.
      pub fn has_varargs(name: &str) -> Option<bool> {
        Some(match name {
          #(#name_str => #has_varargs,)*
          _ => return None,
        })
      }
    }

    #[cfg(feature = "to_tokens")]
    use proc_macro2::TokenStream;
    #[cfg(feature = "to_tokens")]
    use quote::{quote, ToTokens, TokenStreamExt};

    #[cfg(feature = "to_tokens")]
    impl ToTokens for QueryFunc {
      fn to_tokens(&self, _tokens: &mut TokenStream) {
        match self {
          #(Self::#name(#(#arg),*) => {#to_tokens}),*
        }
      }
    }
  };

  // Prints the output, formatted
  // let mut p =
  //   std::process::Command::new("rustfmt").stdin(std::process::Stdio::piped()).
  // spawn().unwrap(); std::io::Write::write_all(p.stdin.as_mut().unwrap(),
  // out.to_string().as_bytes()).unwrap(); p.wait_with_output().unwrap();

  out.into()
}

fn convert_arg(arg: &LitStr) -> Ident {
  Ident::new(
    match arg.value().as_str() {
      "do" => "d",
      "else" => "el",
      "if" => "i",
      "in" => "i",
      "let" => "l",
      "match" => "m",
      "ref" => "r",
      "self" => "slf",
      v => &v,
    },
    Span::call_site(),
  )
}

fn is_option(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Path(p) => p.path.segments.first().unwrap().ident == "Option",
    _ => false,
  }
}
fn is_unit(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Tuple(t) => t.elems.is_empty(),
    _ => false,
  }
}
fn is_varargs(arg: &QueryFuncArg) -> bool {
  match &arg.ty {
    Type::Path(p) => p.path.segments.first().unwrap().ident == "VarArgs",
    _ => false,
  }
}
