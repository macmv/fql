#[macro_use]
extern crate syn;

#[macro_use]
extern crate quote;

use proc_macro::TokenStream;

mod query;

#[proc_macro]
pub fn query_func(input: TokenStream) -> TokenStream {
  query::query_func(input)
}
