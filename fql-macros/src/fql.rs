use fql_parse::Expr;
use proc_macro::{Span, TokenStream};
use proc_macro2::{Span as Span2, TokenStream as TokenStream2};
use quote::{spanned::Spanned, ToTokens};
use std::collections::HashMap;
use syn::{
  parse::{Parse, ParseStream},
  punctuated::Punctuated,
  spanned::Spanned as _,
  token, Ident, LitStr, Result,
};

#[derive(Debug, Clone)]
pub enum FQL {
  Array(token::Bracket, Punctuated<FQL, Token![,]>),
  Struct(token::Brace, Punctuated<FQLMapItem, Token![,]>),
  Func(Ident, token::Paren, Punctuated<FQL, Token![,]>),
  Rust(syn::Expr),
}

#[derive(Debug, Clone)]
pub struct FQLMapItem {
  key:   Ident,
  colon: Token![:],
  val:   FQL,
}

impl Parse for FQLMapItem {
  fn parse(input: ParseStream) -> Result<Self> {
    Ok(FQLMapItem { key: input.parse()?, colon: input.parse()?, val: input.parse()? })
  }
}

impl Parse for FQL {
  fn parse(input: ParseStream) -> Result<Self> {
    if input.peek(syn::Ident) && input.peek2(token::Paren) {
      let args;
      Ok(FQL::Func(
        input.parse()?,
        parenthesized!(args in input),
        args.parse_terminated(FQL::parse)?,
      ))
    } else if input.peek(token::Bracket) {
      let args;
      Ok(FQL::Array(bracketed!(args in input), args.parse_terminated(FQL::parse)?))
    } else if input.peek(token::Brace) {
      let args;
      Ok(FQL::Struct(braced!(args in input), args.parse_terminated(FQLMapItem::parse)?))
    } else {
      Ok(FQL::Rust(input.parse()?))
    }
  }
}

impl ToTokens for FQLMapItem {
  fn to_tokens(&self, stream: &mut TokenStream2) {
    let key = LitStr::new(&self.key.to_string(), self.key.span());
    let val = &self.val;
    stream.extend(quote!((#key.to_string(), #val.into())));
  }
}

impl ToTokens for FQL {
  fn to_tokens(&self, stream: &mut TokenStream2) {
    stream.extend(match self {
      FQL::Array(brackets, fields) => {
        quote!(::fql_parse::Expr::Array(vec![#fields].into_iter().collect()))
      }
      FQL::Struct(braces, fields) => {
        quote!(::fql_parse::Expr::Object([#fields].into_iter().collect()))
      }
      FQL::Func(name, parens, args) => {
        if let Some(res) = convert_args(&name.to_string(), parens.span, args) {
          match res {
            Ok(args) => {
              // Given the position of name to all these tokens (produces better errors).
              let variant = quote_spanned!(name.span() => ::fql_parse::QueryFunc::#name);
              quote!(::fql_parse::Expr::from(#variant(#args)))
            }
            Err(err) => err,
          }
        } else {
          // Not a query function, so it's probably a normal function call.
          quote!(::fql_parse::Expr::from(#name(#args)))
        }
      }
      FQL::Rust(expr) => {
        quote!(::fql_parse::Expr::from(#expr))
      }
    });
  }
}

// Returns None if an invalid name. Returns Some(Ok()) if these are valid args
// and a valid name. Returns Some(Err()) if the number of arguments is wrong.
fn convert_args(
  name: &str,
  span: Span2,
  args: &Punctuated<FQL, Token![,]>,
) -> Option<std::result::Result<TokenStream2, TokenStream2>> {
  let expected = fql_parse::QueryFunc::arg_count(name);
  let expected = match expected {
    Some(v) => v,
    None => return None,
  };
  Some({
    let num_optional = fql_parse::QueryFunc::optional(name).unwrap();
    let num_non_optional = expected - num_optional;
    let no_args = fql_parse::QueryFunc::no_args(name).unwrap();
    let has_varargs = fql_parse::QueryFunc::has_varargs(name).unwrap();
    if no_args {
      if args.len() == 0 {
        Ok(quote_spanned!(span => ()))
      } else {
        let msg = format!(
          "expected 0 arguments, found {} arguments\nhelp: see the docs: {}",
          args.len(),
          fql_parse::QueryFunc::doc_link(name).unwrap()
        );
        Err(quote_spanned!(span => compile_error!(#msg)))
      }
    } else if has_varargs {
      let num_non_optional = expected - 1;
      if args.len() < num_non_optional {
        let msg = format!(
          "expected at least {} arguments, found {} arguments\nhelp: see the docs: {}",
          num_non_optional,
          args.len(),
          fql_parse::QueryFunc::doc_link(name).unwrap()
        );
        Err(quote_spanned!(span => compile_error!(#msg)))
      } else {
        let mut iter = args.iter();
        let mut out = quote!();
        for _ in 0..num_non_optional {
          let item = iter.next().unwrap();
          out.extend(quote!(#item,));
        }
        out.extend(quote!(::fql_parse::VarArgs::new(vec![#(#iter),*])));
        Ok(out)
      }
    } else if num_non_optional == expected {
      if args.len() != expected {
        let msg = format!(
          "expected {} arguments, found {} arguments\nhelp: see the docs: {}",
          expected,
          args.len(),
          fql_parse::QueryFunc::doc_link(name).unwrap()
        );
        Err(quote_spanned!(span => compile_error!(#msg)))
      } else {
        Ok(quote!(#args))
      }
    } else {
      let mut iter = args.iter();
      let mut out = quote!();
      if args.len() < num_non_optional || args.len() > expected {
        let len = args.len();
        let msg = format!(
          "expected {} to {} arguments, found {} arguments\nhelp: see the docs: {}",
          num_non_optional,
          expected,
          args.len(),
          fql_parse::QueryFunc::doc_link(name).unwrap()
        );
        return Some(Err(quote_spanned!(span => compile_error!(#msg))));
      } else {
        for _ in 0..num_non_optional {
          let item = iter.next().unwrap();
          out.extend(quote!(#item,));
        }
        for _ in 0..num_optional {
          let item = iter.next();
          match item {
            Some(v) => out.extend(quote!(Some(#item),)),
            None => out.extend(quote!(None,)),
          }
        }
      }
      Ok(out)
    }
  })
}

pub fn fql(input: TokenStream) -> TokenStream {
  let fql = parse_macro_input!(input as FQL);
  let out = quote!(#fql);

  // Prints the output, formatted

  // let mut p =
  //   std::process::Command::new("rustfmt").stdin(std::process::Stdio::piped()).
  // spawn().unwrap(); std::io::Write::write_all(
  //   p.stdin.as_mut().unwrap(),
  //   format!("fn main() {{ let v = {}; }}", out).as_bytes(),
  // )
  // .unwrap();
  // p.wait_with_output().unwrap();

  out.into()
}
