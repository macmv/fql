#![feature(proc_macro_span)]

use fql_parse::parse;
use proc_macro::{Span, TokenStream};
use std::fs;
use syn::LitStr;

#[macro_use]
extern crate syn;

#[macro_use]
extern crate quote;

mod fql;

#[proc_macro]
pub fn fql_at(input: TokenStream) -> TokenStream {
  let name = parse_macro_input!(input as LitStr);
  // Get the source directory this was called from, and add the input string to
  // that (same as what include_str does).
  let path = Span::call_site().source_file().path().parent().unwrap().join(name.value());
  let src = match fs::read_to_string(&path) {
    Ok(s) => s,
    Err(_) => return quote!(compile_error!("could not find file")).into(),
  };
  let out = match parse::parse_expr(&src) {
    Ok(e) => quote!(#e),
    Err(e) => {
      eprint!("{}", e.gen(&src, &path, true));
      quote!(compile_error!("error parsing input (see the error above)"))
    }
  };

  // Prints the output, formatted

  // let mut p =
  //   std::process::Command::new("rustfmt").stdin(std::process::Stdio::piped()).
  // spawn().unwrap(); std::io::Write::write_all(
  //   p.stdin.as_mut().unwrap(),
  //   format!("fn main() {{ let v = {}; }}", out).as_bytes(),
  // )
  // .unwrap();
  // p.wait_with_output().unwrap();

  out.into()
}

#[proc_macro]
pub fn fql(input: TokenStream) -> TokenStream {
  fql::fql(input)
}
