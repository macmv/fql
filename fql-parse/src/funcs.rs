use super::Expr;
use serde::{ser::SerializeMap, Serialize, Serializer};
use serde_json::{json, to_value};
use std::collections::HashMap;

fql_build_macros::query_func! {
  // Basic functions

  Ref("ref": Expr, "id": Option<Expr>) {
    if let Some(id) = id {
      out.insert("ref", to_value(r).unwrap());
      out.insert("id", to_value(id).unwrap());
    } else {
      out.insert("@ref", to_value(r).unwrap());
    }
  },
  Abort("abort": Expr),
  At("at": Expr, "expr": Expr),
  Let("let": Expr, "in": Expr) {
    match l {
      Expr::Object(bindings) => {
        out.insert(
          "let",
          json!(
            bindings
            .iter()
            .map(|(k, v)| {
              let mut map = HashMap::new();
              map.insert(k, to_value(v).unwrap());
              map
            })
            .collect::<Vec<_>>()
          ));
        out.insert("in", to_value(i).unwrap());
      },
      _ => {
        // NOTE: This is what the javascript driver does, although this
        // seems invalid. This essentially allows this syntax:
        //
        // ```
        // Let(5, Do(...))
        // ```
        //
        // The above makes no sense, and should produce an error (in my
        // opinion). However, I do not have error production setup, so
        // this currently just produces an empty variables table (just
        // like the javascript driver).
        out.insert("let", json!([]));
        out.insert("in", to_value(i).unwrap());
        // panic!("cannot pass non-object to Let");
      }
    }
  },
  Var("var": Expr),
  If("if": Expr, "then": Expr, "else": Expr),
  Do("do": VarArgs<Expr>) {
    // Oddity with the wire protocol; this is always serialized as an array.
    // Nothing else appears to do this, as everything else uses the `varargs`
    // function to serialize arguments (which makes a 1 length array into the
    // one value of that array, and anything else into the array itself).
    out.insert("do", json!(d.inner()));
  },
  Lambda("lambda": Expr, "expr": Expr),
  Call("call": Expr, "arguments": VarArgs<Expr>),
  Query("query": Expr),

  // Collection functions

  // Map and ForEach don't follow the norm; they use `collection` as the name
  // for the first argument, even though most other functions don't. This (once
  // again) matches the javascript driver :P
  Map("collection": Expr, "map": Expr),
  ForEach("collection": Expr, "foreach": Expr),
  Filter("collection": Expr, "filter": Expr),
  // These functions are back to normal
  Take("take": Expr, "collection": Expr),
  Drop("drop": Expr, "collection": Expr),
  Prepend("prepend": Expr, "collection": Expr),
  Append("append": Expr, "collection": Expr),
  IsEmpty("is_empty": Expr),
  IsNonEmpty("is_nonempty": Expr),
  IsNumber("is_number": Expr),
  IsDouble("is_double": Expr),
  IsInteger("is_integer": Expr),
  IsBoolean("is_boolean": Expr),
  IsNull("is_null": Expr),
  IsBytes("is_bytes": Expr),
  IsTimestamp("is_timestamp": Expr),
  IsDate("is_date": Expr),
  IsString("is_string": Expr),
  IsArray("is_array": Expr),
  IsObject("is_object": Expr),
  IsRef("is_ref": Expr),
  IsSet("is_set": Expr),
  IsDoc("is_doc": Expr),
  IsLambda("is_lambda": Expr),
  IsCollection("is_collection": Expr),
  IsDatabase("is_database": Expr),
  IsIndex("is_index": Expr),
  IsFunction("is_function": Expr),
  IsKey("is_key": Expr),
  IsToken("is_token": Expr),
  IsCredentials("is_credentials": Expr),
  IsRole("is_role": Expr),

  // Read functions

  Get("get": Expr, "ts": Option<Expr>),
  KeyFromSecret("key_from_secret": Expr),
  Reduce("reduce": Expr, "initial": Expr, "collection": Expr),
  Paginate("paginate": Expr, "size": Option<Expr>, "after": Option<Expr>, "before": Option<Expr>, "sources": Option<Expr>),
  Exists("exists": Expr, "ts": Option<Expr>),

  // Write functions

  Create("create": Expr, "params": Expr),
  Update("update": Expr, "params": Expr),
  Replace("replace": Expr, "params": Expr),
  Delete("delete": Expr),
  Insert("insert": Expr, "ts": Expr, "action": Expr, "params": Expr),
  Remove("remove": Expr, "ts": Expr, "action": Expr),
  CreateCollection("create_collection": Expr),
  CreateDatabase("create_database": Expr),
  CreateIndex("create_index": Expr),
  CreateKey("create_key": Expr),
  CreateFunction("create_function": Expr),
  CreateRole("create_role": Expr),
  CreateAccessProvider("create_aaccess_provider": Expr),

  // Sets

  Singleton("singleton": Expr),
  Events("events": Expr),
  Match("match": Expr, "terms": VarArgs<Expr>),
  Union("union": VarArgs<Expr>),
  Merge("merge": Expr, "with": Expr, "lambda": Option<Expr>),
  Intersection("intersection": VarArgs<Expr>),
  Difference("difference": VarArgs<Expr>),
  Distinct("distinct": Expr),
  Join("join": Expr, "with": Expr),
  Range("range": Expr, "from": Expr, "to": Expr),

  // Authentication

  Login("login": Expr, "params": Expr),
  Logout("logout": Expr),
  Identify("identify": Expr, "password": Expr),
  CurrentIdentity("current_identity": ()),
  HasCurrentIdentity("has_current_identity": ()),
  CurrentToken("current_token": ()),
  HasCurrentToken("has_current_token": ()),

  // String functions

  Concat("concat": Expr, "separator": Option<Expr>),
  Casefold("casefold": Expr, "normalizer": Option<Expr>),
  ContainsStr("containsstr": Expr, "search": Expr),
  ContainsStrRegex("containsstrregex": Expr, "pattern": Expr),
  StartsWith("startswith": Expr, "search": Expr),
  EndsWith("endswith": Expr, "search": Expr),
  RegexEscape("regexescape": Expr),
  FindStr("findstr": Expr, "find": Expr, "start": Option<Expr>),
  FindStrRegex("findstrregex": Expr, "pattern": Expr, "start": Option<Expr>, "num_results": Option<Expr>),
  Length("length": Expr),
  Lowercase("lowercase": Expr),
  LTrim("ltrim": Expr),
  NGram("ngram": Expr, "min": Option<Expr>, "max": Option<Expr>),
  Repeat("repeat": Expr, "number": Option<Expr>),
  ReplaceStr("replacestr": Expr, "find": Expr, "replace": Expr),
  ReplaceStrRegex("replacestrregex": Expr, "pattern": Expr, "replace": Expr, "first": Option<Expr>),
  RTrim("rtrim": Expr),
  Space("space": Expr),
  SubString("substring": Expr, "start": Expr, "length": Option<Expr>),
  TitleCase("titlecase": Expr),
  Trim("trim": Expr),
  Uppercase("uppercase": Expr),
  Format("format": Expr, "values": VarArgs<Expr>),

  // Time functions

  Time("time": Expr),
  Epoch("epoch": Expr, "unit": Expr),
  TimeAdd("time_add": Expr, "offset": Expr, "unit": Expr),
  TimeSubtract("time_subtract": Expr, "offset": Expr, "unit": Expr),
  TimeDiff("time_diff": Expr, "other": Expr, "unit": Expr),
  Date("date": Expr),
  Now("now": Expr),

  // Misc functions

  NextId("next_id": ()),
  NewId("new_id": ()),

  Database("database": Expr, "scope": Option<Expr>),
  Index("index": Expr, "scope": Option<Expr>),
  Collection("collection": Expr, "scope": Option<Expr>),
  Function("function": Expr, "scope": Option<Expr>),
  Role("role": Expr, "scope": Option<Expr>),

  AccessProviders("access_providers": Expr),
  Collections("collections": Expr),
  Databases("databases": Expr),
  Indexes("indexes": Expr),
  Functions("functions": Expr),
  Roles("roles": Expr),
  Keys("keys": Expr),
  Tokens("tokens": Expr),
  Credentials("credentials": Expr),

  // Conditional functions (considered misc)

  Equals("equals": VarArgs<Expr>),
  Contains("contains": Expr, "in": Expr),
  ContainsValue("contains_value": Expr, "in": Expr),
  ContainsField("contains_field": Expr, "in": Expr),
  ContainsPath("contains_path": Expr, "in": Expr),
  Select("select": Expr, "from": Expr, "default": Option<Expr>),
  SelectAll("select_all": Expr, "from": Expr),

  // Math

  Abs("abs": Expr),
  Add("add": VarArgs<Expr>),
  BitAnd("bitand": VarArgs<Expr>),
  BitNot("bitnot": VarArgs<Expr>),
  BitOr("bitor": VarArgs<Expr>),
  BitXor("bitxor": VarArgs<Expr>),
  Ceil("ceil": Expr),
  Divide("divide": VarArgs<Expr>),
  Floor("floor": Expr),
  Max("max": VarArgs<Expr>),
  Min("min": VarArgs<Expr>),
  Modulo("modulo": VarArgs<Expr>),
  Multiply("multiply": VarArgs<Expr>),
  Round("round": Expr, "precision": Option<Expr>),
  Subtract("subtract": VarArgs<Expr>),
  Sign("sign": Expr),
  Sqrt("sqrt": Expr),
  Trunc("trunc": Expr, "precision": Option<Expr>),

  Count("count": Expr),
  Sum("sum": Expr),
  Mean("mean": Expr),
  Any("any": Expr),
  All("all": Expr),
  Acos("acos": Expr),
  Asin("asin": Expr),
  Atan("atan": Expr),
  Cos("cos": Expr),
  Cosh("num": Expr),
  Degrees("degrees": Expr),
  Exp("exp": Expr),
  Hypot("hypot": Expr, "b": Expr),
  Ln("ln": Expr),
  Log("log": Expr),
  Pow("pow": Expr, "exp": Option<Expr>),
  Radians("radians": Expr),
  Sin("sin": Expr),
  Sinh("sinh": Expr),
  Tan("tan": Expr),
  Tanh("tanh": Expr),

  // Logical functions

  LT("lt": VarArgs<Expr>),
  LTE("lte": VarArgs<Expr>),
  GT("gt": VarArgs<Expr>),
  GTE("gte": VarArgs<Expr>),
  And("and": VarArgs<Expr>),
  Or("or": VarArgs<Expr>),
  Not("not": Expr),
  ToString("to_string": Expr),
  ToNumber("to_number": Expr),
  ToObject("to_object": Expr),
  ToArray("to_array": Expr),
  ToDouble("to_double": Expr),
  ToInteger("to_integer": Expr),
  ToTime("to_time": Expr),
  ToSeconds("to_seconds": Expr),
  ToMillis("to_millis": Expr),
  ToMicros("to_micros": Expr),

  // More time functions

  DayOfWeek("day_of_week": Expr),
  DayOfYear("day_of_year": Expr),
  DayOfMonth("day_of_month": Expr),
  Hour("hour": Expr),
  Minute("minute": Expr),
  Second("second": Expr),
  Month("month": Expr),
  Year("year": Expr),
  ToDate("to_date": Expr),

  // Functions in the wrong order? This is 1:1 with the JS driver, so I'm leaving them like this.

  MoveDatabase("move_database": Expr, "to": Expr),
  Documents("documents": Expr),
  Reverse("reverse": Expr),
  AccessProvider("access_provider": Expr, "scope": Option<Expr>),
}

pub struct VarArgs<T> {
  inner: Vec<T>,
}

impl<T> Serialize for VarArgs<T>
where
  T: Serialize,
{
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    // This matches the python impl of variable length args
    if self.inner.len() == 1 {
      self.inner[0].serialize(serializer)
    } else {
      self.inner.serialize(serializer)
    }
  }
}

impl<T> VarArgs<T> {
  pub fn new(inner: Vec<T>) -> Self {
    VarArgs { inner }
  }

  pub fn inner(&self) -> &[T] {
    &self.inner
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn it_works() {
    let func = QueryFunc::Create(
      QueryFunc::Ref(QueryFunc::Collection("hello".into(), None).into(), None).into(),
      serde_json::json!({"data": {"name": "Orwen"}}).into(),
    );
    println!("{}", serde_json::to_string(&func).unwrap());
  }
}
