use super::token::{ParseError, Punct, Result, Tokenizer};
use crate::{Expr, QueryFunc, VarArgs};
use std::{collections::HashMap, io};

pub struct Parser<R> {
  tokens: Tokenizer<R>,
}

impl<R> Parser<R> {
  pub fn new(tokens: Tokenizer<R>) -> Self {
    Parser { tokens }
  }

  pub fn tokens(&mut self) -> &mut Tokenizer<R> {
    &mut self.tokens
  }
}

pub trait Parse {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self>
  where
    Self: Sized;
}

impl<R: io::Read> Parser<R> {
  pub fn parse<T: Parse>(&mut self) -> Result<T> {
    T::parse(self)
  }
}

impl Parse for () {
  fn parse<R: io::Read>(_p: &mut Parser<R>) -> Result<Self> {
    Ok(())
  }
}

impl Parse for Expr {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    match p.tokens().peek(0)? {
      // A function call
      t if t.is_ident() => {
        let ident = p.tokens().read().unwrap().ident("").unwrap();
        p.tokens().expect(Punct::OpenParen)?;
        let func = QueryFunc::parse(ident, p)?;
        p.tokens().expect(Punct::CloseParen)?;
        Ok(Expr::Func(Box::new(func)))
      }
      // A number or string
      t if t.is_lit() => {
        let lit = p.tokens().read().unwrap().lit("").unwrap();
        if lit.is_int() {
          Ok(Expr::Int(lit.unwrap_int()))
        } else if lit.is_float() {
          Ok(Expr::Float(lit.unwrap_float()))
        } else if lit.is_str() {
          Ok(Expr::String(lit.unwrap_str()))
        } else {
          unreachable!()
        }
      }
      // An object
      t if t.is_punct(Punct::OpenBrace) => {
        p.tokens().read().unwrap();
        let mut out = HashMap::new();
        loop {
          let key = p.tokens().read()?.ident("an object key")?.into();
          p.tokens.expect(Punct::Colon)?;
          let val = p.parse()?;
          out.insert(key, val);
          let found_comma = p.tokens().peek(0)?.is_punct(Punct::Comma);
          if found_comma {
            p.tokens().read().unwrap();
          }
          if p.tokens().peek(0)?.is_punct(Punct::CloseBrace) {
            p.tokens().read().unwrap();
            break;
          }
          if !found_comma {
            p.tokens().expect(Punct::Comma)?;
          }
        }
        Ok(Expr::Object(out))
      }
      t => Err(ParseError::unexpected(t.clone(), "an expression")),
    }
  }
}

impl Parse for VarArgs<Expr> {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    let mut out = vec![];
    while !p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
      out.push(p.parse()?);

      let found_comma = p.tokens().peek(0)?.is_punct(Punct::Comma);
      if found_comma {
        p.tokens().read().unwrap();
      }
      if p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
        break;
      }
      if !found_comma {
        p.tokens().expect(Punct::Comma)?;
      }
    }
    Ok(VarArgs::new(out))
  }
}

impl Parse for Option<Expr> {
  fn parse<R: io::Read>(p: &mut Parser<R>) -> Result<Self> {
    if p.tokens().peek(0)?.is_punct(Punct::CloseParen) {
      Ok(None)
    } else {
      p.parse().map(|v| Some(v))
    }
  }
}
