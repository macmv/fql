mod parser;
pub mod token;

pub use parser::Parser;

use super::Expr;
use token::{Result, Tokenizer};

pub fn parse_expr(text: &str) -> Result<Expr> {
  let mut parser = Parser::new(Tokenizer::new(text.as_bytes()));
  parser.parse()
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn simple_expr() {
    // parse_expr(r#"Create(2, 3)"#).unwrap();
    parse_expr(r#"Ref(3)"#).unwrap();
    parse_expr(r#"Create(Ref(Collection("hello")), 5)"#).unwrap();
    let expr = parse_expr(
      r#"Create(Ref(Collection("hello"), "2"), {
      data: {
        name: "Orwen",
      }
    })"#,
    )
    .map_err(|e| println!("err: {}", e))
    .unwrap();
    println!("{}", serde_json::to_string(&expr).unwrap());
    panic!();
  }
}
