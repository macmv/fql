//! The tokenizer. This handles individual tokens. Specifically, the
//! [`Tokenizer`] makes it easy to [`read`](Tokenizer::read),
//! [`peek`](Tokenizer::peek), and [`unread`](Tokenizer::unread) tokens.
//!
//! Example of the tokenizer:
//! ```
//! use sugarlang::parse::token::{Punct, Token, Tokenizer};
//!
//! let mut tok = Tokenizer::new("if 5 == 6 {".as_bytes(), 0);
//! assert!(tok.peek(0).unwrap().is_keyword(Punct::If));
//!
//! let t = tok.read().unwrap();
//! assert!(t.is_keyword(Punct::If));
//! assert!(matches!(tok.peek(0).unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
//!
//! // After unreading a token, the next peek or read will find that token again.
//! tok.unread(t);
//! assert!(tok.peek(0).unwrap().is_keyword(Punct::If));
//! assert!(tok.read().unwrap().is_keyword(Punct::If));
//!
//! assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 5));
//! assert!(tok.read().unwrap().is_keyword(Punct::Eq));
//! assert!(matches!(tok.read().unwrap(), Token::Literal(l) if l.unwrap_int() == 6));
//!
//! // This is the same thing as read() then is_keyword(). This will return a parse error
//! // if the token is not the right keyword.
//! tok.expect(Punct::OpenBrace).unwrap();
//!
//! // Now that we are out of tokens, `read` will return EOF, and `read_opt` will return None.
//! // EOF is a token so that errors on the cli are easier to work with. For example, we
//! // can expected a `)` token, but get an EOF token instead. It just makes things simpler.
//! assert!(tok.read_opt().unwrap().is_none());
//! assert_eq!(tok.read(), Ok(Token::EOF(0)));
//! ```
mod err;
pub use err::{ParseError, Result};

use std::{borrow::Borrow, collections::VecDeque, fmt, io, io::Read, ops::Deref, str::FromStr};

mod pos;
mod punct;
mod reader;
#[cfg(test)]
mod tests;

pub use pos::{Pos, Span};
pub use punct::Punct;
use reader::PeekReader;

/// The tokenizer. See the [`module`](crate::parse::token) level docs for more.
pub struct Tokenizer<R> {
  reader: PeekReader<R>,
  peeked: VecDeque<Token>,
}

/// A single token. This is the result of [`peek`](Tokenizer::peek) or
/// [`read`](Tokenizer::peek) on the tokenizer. This contains a token kind, and
/// a span. It can be used to generate errors (for unexpected tokens) or to
/// validate that a keyword is present.
#[derive(Debug, Clone, PartialEq)]
pub enum Token {
  Punct(PunctPos),
  Ident(Ident),
  Literal(LiteralPos),
  /// A special token. This is only emitted once the tokenizer has read all the
  /// bytes in the file. This is used so that error handling is simpler for
  /// the cli.
  EOF,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Ident {
  inner: String,
  pos:   Span,
}

#[derive(Debug, Clone, PartialEq)]
pub struct PunctPos {
  inner: Punct,
  pos:   Span,
}

#[derive(Debug, Clone, PartialEq)]
pub struct LiteralPos {
  inner: Literal,
  pos:   Span,
}

#[derive(Debug, Clone, PartialEq)]
enum Literal {
  Int(i64),
  Float(f64),
  String(String),
}

impl fmt::Display for Token {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Punct(v) => write!(f, "`{}`", v.inner),
      Self::Ident(v) => write!(f, "`{}`", v.inner),
      Self::Literal(v) => write!(f, "{}", v.inner),
      Self::EOF => write!(f, "EOF"),
    }
  }
}

impl fmt::Display for Literal {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::Int(v) => write!(f, "`{}`", v),
      Self::Float(v) => write!(f, "`{}`", v),
      Self::String(v) => write!(f, "\"{}\"", v),
    }
  }
}

impl AsRef<str> for Ident {
  fn as_ref(&self) -> &str {
    &self.inner
  }
}
impl Borrow<String> for Ident {
  fn borrow(&self) -> &String {
    &self.inner
  }
}
impl From<Ident> for String {
  fn from(v: Ident) -> String {
    v.inner
  }
}
impl Deref for Ident {
  type Target = str;

  fn deref(&self) -> &str {
    &self.inner
  }
}
impl Ident {
  pub fn pos(&self) -> Span {
    self.pos
  }
}

impl AsRef<Punct> for PunctPos {
  fn as_ref(&self) -> &Punct {
    &self.inner
  }
}
impl Borrow<Punct> for PunctPos {
  fn borrow(&self) -> &Punct {
    &self.inner
  }
}
impl PunctPos {
  pub fn pos(&self) -> Span {
    self.pos
  }
  pub fn value(&self) -> &Punct {
    &self.inner
  }
}

impl LiteralPos {
  pub fn pos(&self) -> Span {
    self.pos
  }
  pub fn is_int(&self) -> bool {
    matches!(self.inner, Literal::Int(_))
  }
  pub fn is_float(&self) -> bool {
    matches!(self.inner, Literal::Float(_))
  }
  pub fn is_str(&self) -> bool {
    matches!(self.inner, Literal::String(_))
  }

  pub fn unwrap_int(&self) -> i64 {
    match self.inner {
      Literal::Int(v) => v,
      _ => panic!("unwrapped on non-int {:?}", self),
    }
  }
  pub fn unwrap_float(&self) -> f64 {
    match self.inner {
      Literal::Float(v) => v,
      _ => panic!("unwrapped on non-int {:?}", self),
    }
  }
  pub fn unwrap_str(self) -> String {
    match self.inner {
      Literal::String(v) => v,
      _ => panic!("unwrapped on non-int {:?}", self),
    }
  }
}
impl Ident {
  pub fn new<N: Into<String>>(name: N, pos: Span) -> Self {
    Ident { inner: name.into(), pos }
  }

  pub fn value(&self) -> &str {
    &self.inner
  }
}
macro_rules! unwrap_tok {
  ( $name:ident, $val:ident, $ret:ty ) => {
    pub fn $name<M: Into<String>>(self, msg: M) -> Result<$ret> {
      match self {
        Token::$val(v) => Ok(v),
        v => Err(ParseError::unexpected(v, msg)),
      }
    }
  };
}

impl Token {
  pub fn pos(&self) -> Span {
    match self {
      Token::Punct(v) => v.pos,
      Token::Ident(v) => v.pos,
      Token::Literal(v) => v.pos,
      Token::EOF => Span::eof(),
    }
  }
  // Unwraps the token. If it is not a punctuator, this will produce a
  // ParseError::unexpected, with the message being the message passed to this
  // function.
  unwrap_tok!(punct, Punct, PunctPos);
  // Unwraps the token. If it is not an identifier, this will produce a
  // ParseError::unexpected, with the message being the message passed to this
  // function.
  unwrap_tok!(ident, Ident, Ident);
  // Unwraps the token. If it is not an int, this will produce a
  // ParseError::unexpected, with the message being the message passed to this
  // function.
  unwrap_tok!(lit, Literal, LiteralPos);

  pub fn is_punct(&self, p: Punct) -> bool {
    matches!(self, Token::Punct(v) if v.inner == p)
  }
  pub fn is_ident(&self) -> bool {
    matches!(self, Token::Ident(_))
  }
  pub fn is_lit(&self) -> bool {
    matches!(self, Token::Literal(_))
  }
}

macro_rules! try_eof {
  ( $self:expr, $expr:expr ) => {
    match $expr {
      Some(v) => v,
      None => return Ok(Token::EOF),
    }
  };
}

enum ParseIOError {
  ParseError(ParseError),
  IO(io::Error),
}

impl From<io::Error> for ParseIOError {
  fn from(e: io::Error) -> Self {
    ParseIOError::IO(e)
  }
}
impl From<ParseError> for ParseIOError {
  fn from(e: ParseError) -> Self {
    ParseIOError::ParseError(e)
  }
}

impl ParseIOError {
  fn into_parse(self) -> ParseError {
    match self {
      Self::ParseError(e) => e,
      Self::IO(e) => ParseError::io(e),
    }
  }
}

impl<R: Read> Tokenizer<R> {
  pub fn new(reader: R) -> Self {
    Tokenizer { reader: PeekReader::new(reader), peeked: VecDeque::new() }
  }
  /// Sets the position of the reader. This will not change any of the tokens,
  /// but it will change the line/column numbers they are on. This is used in
  /// the cli to generate errors for the right line number.
  pub fn set_pos(&mut self, pos: Pos) {
    self.reader.set_pos(pos);
  }
  /// Returns the current position of the tokenizer.
  pub fn pos(&self) -> Pos {
    self.reader.pos()
  }
  /// Reads a token. If the kind does not match the given keyword, then an
  /// unexpected error is returned.
  pub fn expect(&mut self, punct: Punct) -> Result<PunctPos> {
    let t = self.read()?;
    match t {
      Token::Punct(p) if p.inner == punct => Ok(p),
      _ => Err(ParseError::unexpected(t, format!("{}", punct))),
    }
  }
  /// If needed, this will parse a token ahead of the current reader. This will
  /// not affect the current reading position.
  pub fn peek(&mut self, amount: usize) -> Result<&Token> {
    // Prevents reading extra if we find an EOF token
    if &self.peeked.get(amount) == &Some(&Token::EOF) {
      return Ok(&self.peeked[amount]);
    }
    while self.peeked.len() <= amount {
      let t = match self.read_token() {
        // We only want to push to peeked if it is not EOF. This is because if read_token gives
        // use one EOF, all future reads will give us EOF. However, because of borrowing rules,
        // we need to push a single EOF token onto the peeked stack, so that we can borrow into
        // that.
        Ok(Token::EOF) => {
          self.peeked.push_back(Token::EOF);
          return Ok(&self.peeked[self.peeked.len() - 1]);
        }
        Ok(t) => t,
        Err(e) => return Err(e.into_parse()),
      };
      self.peeked.push_back(t);
    }
    Ok(&self.peeked[amount])
  }
  /// This does the same thing as [`peek`](Self::peek), but returns an Ok(None)
  /// when an EOF error whould have occured.
  pub fn peek_opt(&mut self, amount: usize) -> Result<Option<&Token>> {
    // Prevents reading extra if we find an EOF token
    if &self.peeked.get(amount) == &Some(&Token::EOF) {
      return Ok(None);
    }
    while self.peeked.len() <= amount {
      let t = match self.read_token() {
        Ok(Token::EOF) => {
          self.peeked.push_back(Token::EOF);
          return Ok(None);
        }
        Ok(t) => t,
        Err(e) => return Err(e.into_parse()),
      };
      self.peeked.push_back(t);
    }
    Ok(Some(&self.peeked[amount]))
  }
  /// Parses a token from the internal stream, or returns a peeked token.
  pub fn read(&mut self) -> Result<Token> {
    if let Some(c) = self.peeked.pop_front() {
      Ok(c)
    } else {
      self.read_token().map_err(|e| e.into_parse())
    }
  }
  /// Inserts this into the peek stack. This will cause the next call to read
  /// to return the given token.
  pub fn unread(&mut self, tok: Token) {
    self.peeked.push_front(tok)
  }
  /// This does the same thing as [`read`](Self::read), but returns Ok(None) if
  /// an EOF error would have occured.
  pub fn read_opt(&mut self) -> Result<Option<Token>> {
    if let Some(c) = self.peeked.pop_front() {
      Ok(Some(c))
    } else {
      match self.read_token() {
        Ok(Token::EOF) => Ok(None),
        Ok(t) => Ok(Some(t)),
        Err(e) => Err(e.into_parse()),
      }
    }
  }

  /// Parses a token from the internal stream.
  fn read_token(&mut self) -> std::result::Result<Token, ParseIOError> {
    let mut s = String::new();
    loop {
      let c = try_eof!(self, self.reader.peek(0)?);
      if c == '/' {
        if self.reader.peek(1)? == Some('/') {
          // Line comment
          while self.reader.peek(0)? != Some('\n') {
            if self.reader.read().unwrap() == None {
              return Ok(Token::EOF);
            }
          }
          // Consume the newline
          self.reader.read().unwrap().unwrap();
          continue;
        } else if self.reader.peek(1)? == Some('*') {
          // Block comment
          while !(self.reader.peek(0)? == Some('*') && self.reader.peek(1)? == Some('/')) {
            if self.reader.read().unwrap() == None {
              return Err(ParseError::unexpected(Token::EOF, "a closing block comment").into());
            }
          }
          // Consume the `*` and `/`
          self.reader.read().unwrap().unwrap();
          self.reader.read().unwrap().unwrap();
          continue;
        }
      } else if c.is_whitespace() {
        self.reader.read().unwrap().unwrap();
        continue;
      }
      break;
    }
    let start = self.reader.pos();
    let c = try_eof!(self, self.reader.read()?);
    if c == '"' {
      loop {
        let c = try_eof!(self, self.reader.read()?);
        if c == '"' {
          break;
        }
        s.push(c);
      }
      Ok(Token::Literal(LiteralPos {
        inner: Literal::String(s),
        pos:   Span::new(start, self.reader.pos()),
      }))
    } else if matches!(c, '0'..='9') {
      let mut is_float = false;
      s.push(c);
      while let Some(c) = self.reader.peek(0)? {
        if matches!(c, '0'..='9') {
          self.reader.read().unwrap();
        } else if c == '.' {
          if let Some(c2) = self.reader.peek(1)? {
            if matches!(c2, '0'..='9') {
              // Consume the dot token, we will consome the number on the next loop.
              self.reader.read().unwrap();
              is_float = true;
            } else {
              // 5. is not a float, so we don't set is_float here.
              break;
            }
          } else {
            break;
          }
        } else {
          break;
        }
        s.push(c);
      }
      if is_float {
        Ok(Token::Literal(LiteralPos {
          inner: Literal::Float(f64::from_str(&s).unwrap()),
          pos:   Span::new(start, self.reader.pos()),
        }))
      } else {
        Ok(Token::Literal(LiteralPos {
          inner: Literal::Int(i64::from_str(&s).unwrap()),
          pos:   Span::new(start, self.reader.pos()),
        }))
      }
    } else {
      // It is not a number of or a string literal, so it will be a keyword or
      // identifier.
      s.push(c);
      // Puncts are greedy. If we find a *, we want to look for another *, so that
      // ** will be parsed as an Exp token, and not two Mul tokens.
      //
      // This does not apply, as all our puncts are single character (for now). With
      // an extended FQL syntax, this will probably change (things like || being
      // parsed into an Or call).
      let is_valid_ident = matches!(c, 'a'..='z' | 'A'..='Z');

      loop {
        let c = self.reader.peek(0)?;
        if is_valid_ident {
          if !matches!(c, Some('a'..='z' | 'A'..='Z' | '0'..='9' | '_')) {
            // We have reached the end of an identifier
            return Ok(Token::Ident(Ident {
              inner: s,
              pos:   Span::new(start, self.reader.pos()),
            }));
          } else {
            self.reader.read().unwrap();
            if let Some(c) = c {
              // We got another valid identifier character, so push it and keep iterating.
              s.push(c);
            } else {
              // End of the stream, and we have a valid ident
              if let Some(p) = Punct::from_token(&s) {
                return Ok(Token::Punct(PunctPos {
                  inner: p,
                  pos:   Span::new(start, self.reader.pos()),
                }));
              }
              return Ok(Token::Ident(Ident {
                inner: s,
                pos:   Span::new(start, self.reader.pos()),
              }));
            }
          }
        } else {
          // We are trying to match something like >= or ||. Note that we have not pushed
          // c to s yet. This makes sure that single char keywords work as expected.
          if let Some(mut p) = Punct::from_token(&s) {
            loop {
              // s could be `=`, in which case we want to look ahead to see if we have
              // something like `==` or `=>`. Since we haven't pushed c to s,
              // the first iteration of this loop makes sense.
              //
              // This relies in the fact that we will always have a valid keyword in these
              // steps. So the keyword =:= would not parse, because =: is not a
              // valid keyword. This is fine, because this is much simpler than
              // the recursive solution. I am aware that this is still
              // recursive, because it does string equality checks every iteration,
              // but checking the =:= case would be much more recursion than what is already
              // here.
              s.push(match self.reader.peek(0)? {
                Some(c) => c,
                None => break,
              });
              if let Some(new_kw) = Punct::from_token(&s) {
                self.reader.read().unwrap();
                p = new_kw;
              } else {
                break;
              }
            }
            return Ok(Token::Punct(PunctPos {
              inner: p,
              pos:   Span::new(start, self.reader.pos()),
            }));
          } else {
            // This is because all our non-ident tokens are valid with only one character
            // known. The main reason we don't just push and try again is so
            // that we don't get weird errors with a much of spaces in the
            // token.
            return Err(
              ParseError::invalid(Token::Ident(Ident {
                inner: s,
                pos:   Span::new(start, self.reader.pos()),
              }))
              .into(),
            );
            // If we need to check multi character keywords, replace the above
            // with this: ```
            // if let Some(c) = c {
            //   // Invalid keyword, so we consume and try again
            //   self.reader.read().unwrap();
            //   s.push(c);
            // } else {
            //   // End of the stream, and we have an invalid token and invalid
            // ident   return Err(ParseError::invalid(Token::
            // Ident(Ident {     inner: s,
            //     pos:   Span::new(start, self.reader.pos()),
            //   })));
            // }
            // ```
          }
        }
      }
    }
  }
}
