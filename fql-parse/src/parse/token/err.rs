use super::{Ident, Span, Token};
use std::{error::Error, fmt, io, mem, path::Path};

#[derive(Debug)]
pub enum ParseError {
  // The second item is an index into the list of files store in the
  // Sugarlang env. It is how the error is able to print which file
  // was being read while the error happened.
  IO(io::Error),
  Unexpected(Token, String),
  Undefined(Ident, &'static str),
  Redeclaration(Ident, &'static str),
  Invalid(Token),
  Custom(Span, String),

  /// This is how we join multiple errors during parsing
  Multiple(Vec<ParseError>),
  /// An easy way to make an empty error, for collecting
  /// a list of possible errors.
  Empty,
}

impl PartialEq for ParseError {
  fn eq(&self, other: &Self) -> bool {
    match self {
      Self::IO(_) => false,
      Self::Unexpected(tok, msg) => {
        matches!(other, Self::Unexpected(other_tok, other_msg) if other_tok == tok && other_msg == msg)
      }
      Self::Undefined(ident, name) => {
        matches!(other, Self::Undefined(other_ident, other_name) if other_ident == ident && other_name == name)
      }
      Self::Redeclaration(pos, msg) => {
        matches!(other, Self::Redeclaration(other_pos, other_msg) if other_pos == pos && other_msg == msg)
      }
      Self::Invalid(tok) => {
        matches!(other, Self::Invalid(other_tok) if other_tok == tok)
      }
      Self::Custom(pos, value) => {
        matches!(other, Self::Custom(other_pos, other_value) if other_pos == pos && other_value == value)
      }
      Self::Multiple(vec) => {
        matches!(other, Self::Multiple(other_vec) if other_vec == vec)
      }
      Self::Empty => {
        matches!(other, Self::Empty)
      }
    }
  }
}

impl fmt::Display for ParseError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Self::IO(e) => write!(f, "io error: {}", e),
      Self::Unexpected(tok, msg) => {
        write!(f, "Unexpected token {}, expected {}", tok, msg)
      }
      Self::Undefined(ident, name) => {
        write!(f, "Undefined {} '{}'", name, ident.as_ref())
      }
      Self::Redeclaration(ident, name) => {
        write!(f, "Redeclaration of {} '{}'", name, ident.as_ref())
      }
      Self::Invalid(tok) => {
        write!(f, "Invalid token {}", tok)
      }
      Self::Custom(_, value) => {
        write!(f, "{}", value)
      }
      Self::Multiple(vec) => {
        for err in vec {
          writeln!(f, "{}", err)?;
        }
        Ok(())
      }
      Self::Empty => Ok(()),
    }
  }
}

impl Error for ParseError {}

impl ParseError {
  pub fn pos(&self) -> Span {
    match self {
      Self::IO(_) => Span::default(),
      Self::Unexpected(tok, _) => tok.pos(),
      Self::Undefined(ident, _) => ident.pos(),
      Self::Redeclaration(ident, _) => ident.pos(),
      Self::Invalid(tok) => tok.pos(),
      Self::Custom(pos, _) => *pos,
      Self::Multiple(_) => Span::default(),
      Self::Empty => Span::default(),
    }
  }
  pub fn print(&self, src: &str, path: &Path, color: bool) {
    match self {
      Self::IO(_) => println!("failed to read `{}`: {}", path.display(), self),
      Self::Multiple(vec) => {
        let mut iter = vec.iter();
        if let Some(err) = iter.next() {
          err.print(src, path, color);
        }
        for err in iter {
          println!();
          err.print(src, path, color);
        }
      }
      Self::Empty => {}
      _ => {
        print!("{}", self.pos().underline(src, path.to_str().unwrap(), &format!("{}", self), color))
      }
    }
  }
  pub fn gen(&self, src: &str, path: &Path, color: bool) -> String {
    match self {
      Self::IO(_) => format!("failed to read `{}`: {}", path.display(), self),
      Self::Multiple(vec) => {
        let mut out = String::new();
        let mut iter = vec.iter();
        if let Some(err) = iter.next() {
          out += &err.gen(src, path, color);
        }
        for err in iter {
          out += "\n";
          out += &err.gen(src, path, color);
        }
        out
      }
      Self::Empty => String::new(),
      _ => {
        format!(
          "{}",
          self.pos().underline(src, path.to_str().unwrap(), &format!("{}", self), color)
        )
      }
    }
  }
}

impl ParseError {
  pub fn io(e: io::Error) -> Self {
    ParseError::IO(e)
  }
  pub fn unexpected<M: Into<String>>(t: Token, msg: M) -> Self {
    ParseError::Unexpected(t, msg.into())
  }
  pub fn undefined(name: &'static str, ident: Ident) -> Self {
    ParseError::Undefined(ident, name)
  }
  pub fn redeclaration(name: &'static str, ident: Ident) -> Self {
    ParseError::Redeclaration(ident, name)
  }
  pub fn invalid(tok: Token) -> Self {
    ParseError::Invalid(tok)
  }
  pub fn custom(value: String, pos: Span) -> Self {
    ParseError::Custom(pos, value)
  }
  /// Merges the current and other error into one parse error.
  /// If either errors is a Multiple, then the other error will
  /// be added to that list, and Self will be set to that multiple.
  pub fn join(&mut self, mut other: ParseError) {
    if *self == ParseError::Empty {
      *self = other;
      return;
    }
    if other == ParseError::Empty {
      return;
    }
    // All of this logic is to prevent nested Multiple errors. While
    // this doesn't fix all situations of nested Multiples, it speeds
    // up error handling if we aren't allocating new Vecs all the time.
    if !matches!(self, ParseError::Multiple(_)) && matches!(other, ParseError::Multiple(_)) {
      // If other is a multi, then we want to swap it with self
      mem::swap(self, &mut other);
    }
    if let ParseError::Multiple(vec) = self {
      // If self is a multi, then we append to self
      if let ParseError::Multiple(mut other_vec) = other {
        // If other is a multi, we merge
        vec.append(&mut other_vec);
      } else {
        vec.push(other);
      }
    } else {
      let mut new = ParseError::Multiple(vec![]);
      mem::swap(self, &mut new);
      if let ParseError::Multiple(vec) = self {
        // New is now the old self
        vec.push(new);
        vec.push(other);
      } else {
        unreachable!();
      }
    }
  }
  /// Attempts to join the given result with this error. If the
  /// result is Ok(_), then nothing happens. Otherwise, the other
  /// error will be joined with the current error.
  pub fn try_join<T>(&mut self, res: Result<T>) {
    match res {
      Ok(_) => {}
      Err(err) => self.join(err),
    }
  }
  /// If self is a ParseError::Empty, or an empty Multiple, then
  /// Ok(ok) is returned. Otherwise, self is returned.
  pub fn into_result<T>(self, ok: T) -> Result<T> {
    match self {
      ParseError::Empty => Ok(ok),
      ParseError::Multiple(vec) if vec.is_empty() => Ok(ok),
      err => Err(err),
    }
  }
}

pub type Result<T> = std::result::Result<T, ParseError>;
