use super::{Expr, QueryFunc};

impl From<QueryFunc> for Expr {
  fn from(func: QueryFunc) -> Expr {
    Expr::Func(Box::new(func))
  }
}

impl From<bool> for Expr {
  fn from(v: bool) -> Expr {
    Expr::Bool(v)
  }
}
impl From<i64> for Expr {
  fn from(v: i64) -> Expr {
    Expr::Int(v)
  }
}
impl From<f64> for Expr {
  fn from(v: f64) -> Expr {
    Expr::Float(v)
  }
}
impl From<&str> for Expr {
  fn from(s: &str) -> Expr {
    Expr::String(s.into())
  }
}

impl From<serde_json::Value> for Expr {
  fn from(val: serde_json::Value) -> Expr {
    (&val).into()
  }
}
