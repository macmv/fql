use super::{Expr, VarArgs};
use proc_macro2::TokenStream;
use quote::{quote, ToTokens, TokenStreamExt};

impl ToTokens for Expr {
  fn to_tokens(&self, tokens: &mut TokenStream) {
    match self {
      Expr::Null => tokens.append_all(quote!(Expr::Null)),
      Expr::Bool(v) => tokens.append_all(quote!(Expr::Bool(#v))),
      Expr::Int(v) => tokens.append_all(quote!(Expr::Int(#v))),
      Expr::Float(v) => tokens.append_all(quote!(Expr::Float(#v))),
      Expr::String(v) => tokens.append_all(quote!(Expr::String(#v.into()))),
      Expr::Array(elems) => tokens.append_all(quote!(Expr::Array(vec![#(#elems),*]))),
      Expr::Object(map) => {
        let len = map.len();
        let keys = map.keys();
        let vals = map.values();
        tokens.append_all(quote! {{
          let mut out = std::collections::HashMap::with_capacity(#len);
          #(out.insert(#keys.to_string(), #vals);)*
          Expr::Object(out)
        }})
      }
      Expr::Func(func) => tokens.append_all(quote!(Expr::Func(Box::new(#func)))),
    }
  }
}

impl<T: ToTokens> ToTokens for VarArgs<T> {
  fn to_tokens(&self, tokens: &mut TokenStream) {
    let elems = &self.inner();
    tokens.append_all(quote!(fql_parse::VarArgs::new(vec![#(#elems),*])));
  }
}
