use serde::{
  ser::{SerializeMap, SerializeSeq},
  Serialize, Serializer,
};
use std::collections::HashMap;

mod funcs;
mod impls;
pub mod parse;
#[cfg(feature = "to_tokens")]
mod to_tokens;

pub use funcs::{QueryFunc, VarArgs};

#[derive(Serialize)]
pub struct CollectionRef {
  collection: String,
}

pub enum Expr {
  Null,
  Bool(bool),
  Int(i64),
  Float(f64),
  String(String),
  Array(Vec<Expr>),
  Object(HashMap<String, Expr>),
  Func(Box<QueryFunc>),
}

impl From<&serde_json::Value> for Expr {
  fn from(val: &serde_json::Value) -> Expr {
    if let Some(num) = val.as_i64() {
      Expr::Int(num)
    } else if let Some(num) = val.as_u64() {
      Expr::Int(num.try_into().expect("u64 does not fit into i64"))
    } else if let Some(num) = val.as_f64() {
      Expr::Float(num)
    } else if let Some(text) = val.as_str() {
      Expr::String(text.into())
    } else if let Some(val) = val.as_bool() {
      Expr::Bool(val)
    } else if val.is_null() {
      Expr::Null
    } else if let Some(val) = val.as_array() {
      Expr::Array(val.into_iter().map(|v| v.into()).collect())
    } else if let Some(val) = val.as_object() {
      Expr::Object(val.into_iter().map(|(k, v)| (k.clone(), v.into())).collect())
    } else {
      unreachable!()
    }
  }
}

impl Serialize for Expr {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: Serializer,
  {
    match self {
      Self::Null => serializer.serialize_unit(),
      Self::Int(v) => serializer.serialize_i64(*v),
      Self::Float(v) => serializer.serialize_f64(*v),
      Self::String(v) => serializer.serialize_str(v),
      Self::Bool(v) => serializer.serialize_bool(*v),
      Self::Array(arr) => {
        let mut seq = serializer.serialize_seq(Some(arr.len()))?;
        for elem in arr {
          seq.serialize_element(elem)?;
        }
        seq.end()
      }
      Self::Object(data) => {
        let mut map = serializer.serialize_map(Some(1))?;
        map.serialize_entry("object", data)?;
        map.end()
      }
      Self::Func(func) => func.serialize(serializer),
    }
  }
}
